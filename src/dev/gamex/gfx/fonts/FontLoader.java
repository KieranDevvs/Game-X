package dev.gamex.gfx.fonts;

import java.awt.Font;
import java.io.InputStream;

public class FontLoader {

	public static Font loadFont(String name) {
		String fName = "/fonts/" + name;
		try {
			InputStream is = FontLoader.class.getResourceAsStream(fName);
			Font font = Font.createFont(Font.TRUETYPE_FONT, is);
			return font;
		} catch (Exception ex) {
			ex.printStackTrace();
			System.err.println(fName + " not loaded.");
		}
		return null;
	}

}
