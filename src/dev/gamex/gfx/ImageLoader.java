package dev.gamex.gfx;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageLoader {
	
	public static BufferedImage loadImage(String folder, String fileName) {
		try {
			return ImageIO.read(ImageLoader.class.getResource("/" + folder + "/" + fileName));
		} catch (IOException e) {
			System.out.println("Error loading image: " + e.getMessage());
			return null;
		}
	}
}
