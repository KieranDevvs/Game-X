package dev.gamex.gfx;

import java.awt.Font;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dev.gamex.gfx.fonts.FontLoader;

public class Assets {
	
	
	public static HashMap<String, BufferedImage> imageList = new HashMap<String, BufferedImage>();
	public static HashMap<String, Font> fontList = new HashMap<String, Font>();
	public static List<BufferedImage> frameIcons =  new ArrayList<BufferedImage>();
	public static BufferedImage frameIcon;

	
	public static void init() {
		loadFonts();
		loadTextures();
		load16x16Icons();
		load32x32Icons();
		loadFrameIcons();
	}
	
	public static void loadTextures() {
		int width = 64;
		int height = 64;
		SpriteContainer _textureContainer = new SpriteContainer(ImageLoader.loadImage("textures", "sheet.png"));
		imageList.put("progressbar_base", ImageLoader.loadImage("textures", "progressbar_base.png"));
		imageList.put("progressbar_load", ImageLoader.loadImage("textures", "progressbar_load.png"));
		imageList.put("background", ImageLoader.loadImage("textures", "background.png"));
		imageList.put("heart_icon", ImageLoader.loadImage("textures", "heart.png"));
		imageList.put("shield_icon", ImageLoader.loadImage("textures", "shield.png"));
		imageList.put("player", ImageLoader.loadImage("textures", "player.png"));
		imageList.put("bullet", ImageLoader.loadImage("projectiles", "bullet.png"));
		imageList.put("grass", ImageLoader.loadImage("textures", "grass.png"));
		imageList.put("grass2", ImageLoader.loadImage("textures", "grass2.png"));
		imageList.put("campfire", ImageLoader.loadImage("textures", "campfire.png"));
		imageList.put("error", ImageLoader.loadImage("textures", "error.png"));
		imageList.put("bunker_left", _textureContainer.getImageSection(128, 0, width, height));
		imageList.put("bunker_right", _textureContainer.getImageSection(192, 0, width, height));						
		imageList.put("tree", _textureContainer.getImageSection(192, 192, width, height));
		imageList.put("npc", _textureContainer.getImageSection(64, 64, width, height));
	}
	
	public static void loadFonts() {
		fontList.put("title_font", FontLoader.loadFont("absender.ttf"));
		fontList.put("text_font", FontLoader.loadFont("visitor1.ttf")); //Size 10 looks the nicest
		fontList.put("testfont", FontLoader.loadFont("28 Days Later.ttf")); 
	}
	
	public static void load16x16Icons() {
		int width = 16;
		int height = 16;
		SpriteContainer _iconContainer = new SpriteContainer(ImageLoader.loadImage("icons/16x16", "sheet.png"));	
	}
	
	public static void load32x32Icons() {
		int width = 32;
		int height = 32;
		SpriteContainer _iconContainer = new SpriteContainer(ImageLoader.loadImage("icons/32x32", "sheet.png"));
		frameIcon = _iconContainer.getImageSection(0, 0, width, height);
	}
	
	public static void loadFrameIcons() {
		frameIcons.add(ImageLoader.loadImage("icons/frame", "x16.png"));
		frameIcons.add(ImageLoader.loadImage("icons/frame", "x32.png"));
	}
	
	public static BufferedImage getImage(String image) {
		if(imageList.get(image) != null) {
			return imageList.get(image);
		}
		return imageList.get("error");
	}
	
}
