package dev.gamex.gfx;

import java.awt.image.BufferedImage;

public class SpriteContainer {
	private BufferedImage _baseImage;
	
	public SpriteContainer(BufferedImage baseImage) {
		this._baseImage = baseImage;
	}
	
	public BufferedImage getImageSection(int x, int y, int width, int height) {
		return _baseImage.getSubimage(x, y, width, height);
	}
}
