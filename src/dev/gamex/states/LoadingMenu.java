package dev.gamex.states;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;

import dev.gamex.Game;
import dev.gamex.gfx.Assets;

public class LoadingMenu extends StateHandler {

	private Game game;
	
	public LoadingMenu(Game game) {
		super(game);
		this.game = game;
	}

	private int percentLoaded = 0;
	private String event = "";
	
	@Override
	public void render(Graphics g, Game game) {
		float widthPercent = (float) (game.getWidth() - 20) / 100;
		g.drawImage(Assets.imageList.get("background"), (game.getWidth() / 2) - (Assets.imageList.get("background").getWidth() / 2), (game.getHeight() / 2) - (Assets.imageList.get("background").getHeight() / 2), Assets.imageList.get("background").getWidth(), Assets.imageList.get("background").getHeight(), null);
		g.drawImage(Assets.imageList.get("progressbar_base"), 10, game.getHeight() - 20, game.getWidth() - 20, 10, null);
		g.drawImage(Assets.imageList.get("progressbar_load"), 10,  game.getHeight() - 20, (int)(widthPercent * percentLoaded), 10, null);
		g.setColor(Color.RED);
		g.setFont(Assets.fontList.get("text_font").deriveFont(Font.PLAIN, 10));
		AffineTransform affinetransform = new AffineTransform();     
		FontRenderContext frc = new FontRenderContext(affinetransform,true,true);
		g.drawString(event, (int)((game.getWidth() / 2) - (Assets.fontList.get("text_font").deriveFont(Font.PLAIN, 10)).getStringBounds(event, frc).getWidth() / 2), game.getHeight() - 33);
		g.setColor(Color.WHITE);
		g.drawString(Integer.toString(percentLoaded) + "%", (int)((game.getWidth() / 2) - (Assets.fontList.get("text_font").deriveFont(Font.PLAIN, 10).getStringBounds(Integer.toString(percentLoaded), frc).getWidth() / 2)), game.getHeight() - 13);
	}

	@Override
	public void update() {
		if(percentLoaded < 100) {
			percentLoaded++;
		}else{
			game.getStateHandler().setState(new MainMenu(game), game.getDisplay().getCanvas());
		}
	}
	
	public void setLoadBar(int percent, String event) {
		this.percentLoaded = percent;
		this.event = event;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
