package dev.gamex.states;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import dev.gamex.Game;
import dev.gamex.entity.player.Player;
import dev.gamex.map.MainMap;
import dev.gamex.minimap.Minimap;

public class GameState extends StateHandler implements MouseListener, MouseMotionListener, KeyListener {

	private Game game;
	
	public GameState(Game game) {
		super(game);
		this.game = game;
		player = game.getPlayer();
		minimap = new Minimap(300, 5, 100, 100, 255, game);
	}
	
	MainMap mainmap = new MainMap(game);
	private Minimap minimap;
	private Player player;

	@Override
	public void render(Graphics g, Game game) {
		game.getMapHandler().getMap().render(g, game);
		player.render(g, game);
		game.getLightingHandler().render(g);
		minimap.render(g);
	}

	@Override
	public void update() {
		game.getLightingHandler().update();
		player.update();
		game.getGameCamera().focusOnEntity(player);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == 27) {
			game.getStateHandler().setState(new MainMenu(game), game.getDisplay().getCanvas());
		}
		
		if(e.getKeyCode() == 45) { //minus key
			game.setScale(game.getScale() - .10F);
		}
		
		if(e.getKeyCode() == 61) { //plus key
			game.setScale(game.getScale() + .10F);
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		game.setMousePosition(new Point(e.getX(), e.getY()));
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		game.setMousePosition(new Point(e.getX(), e.getY()));
	}

	@Override
	public void mouseClicked(MouseEvent e) {

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		game.getPlayer().isFiring(true);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		game.getPlayer().isFiring(false);
	}

}
