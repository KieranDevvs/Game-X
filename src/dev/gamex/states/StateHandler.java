package dev.gamex.states;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import dev.gamex.Game;

public abstract class StateHandler implements MouseListener, MouseMotionListener, KeyListener {

	public StateHandler(Game game) {
		
	}
	
	private static StateHandler currentState = null;
	
	public void setState(StateHandler state, Canvas canvas) {
		canvas.removeMouseListener(currentState);
		canvas.removeMouseMotionListener(currentState);
		currentState = state;
		canvas.addMouseListener(state);
		canvas.addMouseMotionListener(state);
	}
	
	public StateHandler getState() {
		return currentState;
	}
	
	public abstract void render(Graphics g, Game game);

	public abstract void update();
	
}
