package dev.gamex.states;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;

import dev.gamex.Game;
import dev.gamex.gfx.Assets;
import dev.gamex.input.KeyboardHandler;
import dev.gamex.utils.Utils;

public class MainMenu extends StateHandler implements MouseListener, MouseMotionListener, KeyListener {
	
	private Game game;
	
	public MainMenu(Game game) {
		super(game);
		this.game = game;
	}

	private Rectangle panel;
	private Rectangle login_button;
	private Rectangle register_button;
	private Rectangle settings_button;
	private Rectangle exit_button;
	
	@Override
	public void render(Graphics g, Game game) {
		AffineTransform affinetransform = new AffineTransform();     
		FontRenderContext frc = new FontRenderContext(affinetransform,true,true);    
		
		g.drawImage(Assets.imageList.get("background"), (game.getWidth() / 2) - (Assets.imageList.get("background").getWidth() / 2), (game.getHeight() / 2) - (Assets.imageList.get("background").getHeight() / 2), Assets.imageList.get("background").getWidth(), Assets.imageList.get("background").getHeight(), null);
		panel = new Rectangle((int)(game.getWidth() / 3), (int)(game.getHeight() / 4), (int)(game.getWidth() / 3), (int)(game.getHeight() / 4) * 2);
		
		
		//Interface background
		g.setColor(new Color(0, 0, 0, 230));
		g.fillRect(panel.x, panel.y, panel.width, panel.height);
		g.setColor(new Color(255, 255, 255, 50));
		g.fillRect(panel.x, panel.y, panel.width, panel.height);
		g.setColor(new Color(255, 0, 0, 255));
		g.drawRect(panel.x, panel.y, panel.width, panel.height);
		
		//Interface title
		g.setFont(Assets.fontList.get("testfont").deriveFont(Font.PLAIN, 24));
		g.drawString("Main Menu", panel.x + ((panel.width / 2) - (int)(Assets.fontList.get("testfont").deriveFont(Font.PLAIN, 24)).getStringBounds("Main Menu", frc).getWidth() / 2), panel.y + 30);
		
		
		//Login button
		login_button = new Rectangle((int)(panel.x + (panel.width / 2) - 80), panel.y + 50, 160, 40);
		if(Utils.inBounds(new Point(game.getMousePosition().x, game.getMousePosition().y), login_button)) {
			g.setColor(new Color(255, 255, 255, 40));
			g.fillRect(login_button.x, login_button.y, login_button.width, login_button.height);
		}
		g.setColor(new Color(255, 0, 0, 255));
		g.drawRect(login_button.x, login_button.y, login_button.width, login_button.height);
		g.drawString("Login", panel.x + ((panel.width / 2) - (int)(Assets.fontList.get("title_font").deriveFont(Font.PLAIN, 24)).getStringBounds("Login", frc).getWidth() / 2), login_button.y + (login_button.height / 2)+ + (int)(Assets.fontList.get("title_font").deriveFont(Font.PLAIN, 24).getStringBounds("Login", frc).getHeight() / 2));
		
		
		//Register button
		register_button = new Rectangle((int)(panel.x + (panel.width / 2) - 80), login_button.y + login_button.height + 10, 160, 40);
		if(Utils.inBounds(new Point(game.getMousePosition().x, game.getMousePosition().y), register_button)) {
			g.setColor(new Color(255, 255, 255, 40));
			g.fillRect(register_button.x, register_button.y, register_button.width, register_button.height);
		}
		g.setColor(new Color(255, 0, 0, 255));
		g.drawRect(register_button.x, register_button.y, register_button.width, register_button.height);
		g.drawString("Register", panel.x + ((panel.width / 2) - (int)(Assets.fontList.get("title_font").deriveFont(Font.PLAIN, 24).getStringBounds("Register", frc).getWidth() / 2)), register_button.y + (register_button.height / 2)+ + (int)(Assets.fontList.get("title_font").deriveFont(Font.PLAIN, 24).getStringBounds("Register", frc).getHeight() / 2));


		//Options button
		settings_button = new Rectangle((int)(panel.x + (panel.width / 2) - 80), register_button.y + register_button.height + 10, 160, 40);
		if(Utils.inBounds(new Point(game.getMousePosition().x, game.getMousePosition().y), settings_button)) {
			g.setColor(new Color(255, 255, 255, 40));
			g.fillRect(settings_button.x, settings_button.y, settings_button.width, settings_button.height);
		}
		g.setColor(new Color(255, 0, 0, 255));
		g.drawRect(settings_button.x, settings_button.y, settings_button.width, settings_button.height);
		g.drawString("Settings", panel.x + ((panel.width / 2) - (int)(Assets.fontList.get("title_font").deriveFont(Font.PLAIN, 24).getStringBounds("Settings", frc).getWidth() / 2)), settings_button.y + (settings_button.height / 2)+ + (int)(Assets.fontList.get("title_font").deriveFont(Font.PLAIN, 24).getStringBounds("Settings", frc).getHeight() / 2));


		//Exit button
		exit_button = new Rectangle((int)(panel.x + (panel.width / 2) - 80), settings_button.y + settings_button.height + 10, 160, 40);
		if(Utils.inBounds(new Point(game.getMousePosition().x, game.getMousePosition().y), exit_button)) {
			g.setColor(new Color(255, 255, 255, 40));
			g.fillRect(exit_button.x, exit_button.y, exit_button.width, exit_button.height);
		}
		g.setColor(new Color(255, 0, 0, 255));
		g.drawRect(exit_button.x, exit_button.y, exit_button.width, exit_button.height);
		g.drawString("Exit", panel.x + ((panel.width / 2) - (int)(Assets.fontList.get("title_font").deriveFont(Font.PLAIN, 24).getStringBounds("Exit", frc).getWidth() / 2)), exit_button.y + (exit_button.height / 2)+ + (int)(Assets.fontList.get("title_font").deriveFont(Font.PLAIN, 24).getStringBounds("Exit", frc).getHeight() / 2));

		
		//Credits
		g.setFont(Assets.fontList.get("text_font").deriveFont(Font.PLAIN, 10));
		g.drawString("GameX Developed by Kieran Devlin 2015", panel.x + ((panel.width / 2) - (int)(Assets.fontList.get("text_font").deriveFont(Font.PLAIN, 10).getStringBounds("GameX Developed by Kieran Devlin 2015", frc).getWidth() / 2)), panel.y + panel.height - 20);
	}
	
	@Override
	public void update() {
		
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if(e.getButton() == KeyboardHandler.MouseButtons.LEFT.getValue()) {
			if(Utils.inBounds(new Point(e.getX(), e.getY()), login_button)) { //login
				game.getStateHandler().setState(new GameState(game), game.getDisplay().getCanvas());
//				game.getOverlayHandler().clearShaders();
//				game.getOverlayHandler().setShader(1, new NightOverlay());
//				game.getOverlayHandler().setShader(2, new HUDOverlay());
				System.out.println("Game state started");
			}
			
			if(Utils.inBounds(new Point(e.getX(), e.getY()), settings_button)) { //settings
				game.getStateHandler().setState(new SettingsMenu(game), game.getDisplay().getCanvas());
			}
			
			if(Utils.inBounds(new Point(e.getX(), e.getY()), exit_button)) {
				System.out.println("Game closing");
				System.exit(0);
			}
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		game.setMousePosition(new Point(e.getX(), e.getY()));
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
