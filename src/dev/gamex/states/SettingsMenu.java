package dev.gamex.states;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

import dev.gamex.Game;
import dev.gamex.Settings;
import dev.gamex.gfx.Assets;
import dev.gamex.input.KeyboardHandler;
import dev.gamex.utils.Utils;

public class SettingsMenu extends StateHandler implements MouseListener, MouseMotionListener, KeyListener {
	
	private Game game;
	
	public SettingsMenu(Game game) {
		super(game);
		this.game = game;
	}
	
	AffineTransform affinetransform = new AffineTransform();     
	FontRenderContext frc = new FontRenderContext(affinetransform, true, true);
	
	private Rectangle panel;
	private Rectangle resolution_button;
	private ArrayList<String> resolutions = new ArrayList<String>();
	{
		resolutions.add("640x360");
		resolutions.add("720x480");
		resolutions.add("800x600");
		resolutions.add("1280x720");
		resolutions.add("1920x1080");
	}
	private Rectangle lighting_button;
	private Rectangle lighting_type_button;
	private Rectangle tile_AA_button;
	private Rectangle player_AA_button;
	private Rectangle vsync_button;
	private Rectangle back_button;
	
	private Font title_font = Assets.fontList.get("testfont");
	private Font text_font = Assets.fontList.get("text_font");
	
	@Override
	public void render(Graphics g, Game game) {

		
		g.drawImage(Assets.imageList.get("background"), (game.getWidth() / 2) - (Assets.imageList.get("background").getWidth() / 2), (game.getHeight() / 2) - (Assets.imageList.get("background").getHeight() / 2), Assets.imageList.get("background").getWidth(), Assets.imageList.get("background").getHeight(), null);
		panel = new Rectangle((int)(game.getWidth() / 3), (int)(game.getHeight() / 8), (int)(game.getWidth() / 3), (int)(game.getHeight() / 8) * 6);
		
		
		//Interface background
		g.setColor(new Color(0, 0, 0, 230));
		g.fillRect(panel.x, panel.y, panel.width, panel.height);
		g.setColor(new Color(255, 255, 255, 50));
		g.fillRect(panel.x, panel.y, panel.width, panel.height);
		g.setColor(new Color(255, 0, 0, 255));
		g.drawRect(panel.x, panel.y, panel.width, panel.height);
		
		//Interface title
		g.setFont(title_font.deriveFont(Font.PLAIN, 24));
		g.drawString("Settings", panel.x + ((panel.width / 2) - (int)(title_font.deriveFont(Font.PLAIN, 24)).getStringBounds("Settings", frc).getWidth() / 2), panel.y + 30);

		//Resolution button
		resolution_button = new Rectangle((int)(panel.x + (panel.width / 14)), panel.y + 50, (panel.x + panel.width) - (panel.x + ((panel.width / 14) * 2)), 40);
		if(Utils.inBounds(new Point(game.getMousePosition().x, game.getMousePosition().y), resolution_button)) {
			g.setColor(new Color(255, 255, 255, 40));
			g.fillRect(resolution_button.x, resolution_button.y, resolution_button.width, resolution_button.height);
		}
		g.setColor(new Color(255, 0, 0, 255));
		g.drawRect(resolution_button.x, resolution_button.y, resolution_button.width, resolution_button.height);
		g.drawString("" + game.getWidth() + "x" + game.getHeight(), panel.x + ((panel.width / 2) - (int)(title_font.deriveFont(Font.PLAIN, 24).getStringBounds("" + game.getWidth() + "x" + game.getHeight(), frc).getWidth() / 2)), resolution_button.y + (resolution_button.height / 2)+ + (int)(title_font.deriveFont(Font.PLAIN, 24).getStringBounds("" + game.getWidth() + "x" + game.getHeight(), frc).getHeight() / 2));
		
		//Lighting toggle button
		lighting_button = new Rectangle((int)(panel.x + (panel.width / 2) - 80), resolution_button.y + resolution_button.height + 10, 160, 40);
		if(Utils.inBounds(new Point(game.getMousePosition().x, game.getMousePosition().y), lighting_button)) {
			g.setColor(new Color(255, 255, 255, 40));
			g.fillRect(lighting_button.x, lighting_button.y, lighting_button.width, lighting_button.height);
		}
		g.setColor(new Color(255, 0, 0, 255));
		g.drawRect(lighting_button.x, lighting_button.y, lighting_button.width, lighting_button.height);
		g.drawString("Lighting " + Settings.LIGHTING_ENABLED, panel.x + ((panel.width / 2) - (int)(title_font.deriveFont(Font.PLAIN, 24).getStringBounds("Lighting " + Settings.LIGHTING_ENABLED, frc).getWidth() / 2)), lighting_button.y + (lighting_button.height / 2)+ + (int)(title_font.deriveFont(Font.PLAIN, 24).getStringBounds("Lighting " + Settings.LIGHTING_ENABLED, frc).getHeight() / 2));
		
		//Lighting type button
		lighting_type_button = new Rectangle((int)(panel.x + (panel.width / 2) - 80), lighting_button.y + lighting_button.height + 10, 160, 40);
		if(Utils.inBounds(new Point(game.getMousePosition().x, game.getMousePosition().y), lighting_type_button)) {
			g.setColor(new Color(255, 255, 255, 40));
			g.fillRect(lighting_type_button.x, lighting_type_button.y, lighting_type_button.width, lighting_type_button.height);
		}
		g.setColor(new Color(255, 0, 0, 255));
		g.drawRect(lighting_type_button.x, lighting_type_button.y, lighting_type_button.width, lighting_type_button.height);
		g.drawString(Settings.ALPHA_LIGHTING_ENABLED ? "Alpha lighting" : "Tile Lighting", panel.x + ((panel.width / 2) - (int)(title_font.deriveFont(Font.PLAIN, 24).getStringBounds(Settings.ALPHA_LIGHTING_ENABLED ? "Alpha lighting" : "Tile Lighting", frc).getWidth() / 2)), lighting_type_button.y + (lighting_type_button.height / 2)+ + (int)(title_font.deriveFont(Font.PLAIN, 24).getStringBounds(Settings.ALPHA_LIGHTING_ENABLED ? "Alpha lighting" : "Tile Lighting", frc).getHeight() / 2));
		
		//Tile Anti Aliasing toggle button
		tile_AA_button = new Rectangle((int)(panel.x + (panel.width / 2) - 80), lighting_type_button.y + lighting_type_button.height + 10, 160, 40);
		if(Utils.inBounds(new Point(game.getMousePosition().x, game.getMousePosition().y), tile_AA_button)) {
			g.setColor(new Color(255, 255, 255, 40));
			g.fillRect(tile_AA_button.x, tile_AA_button.y, tile_AA_button.width, tile_AA_button.height);
		}
		g.setColor(new Color(255, 0, 0, 255));
		g.drawRect(tile_AA_button.x, tile_AA_button.y, tile_AA_button.width, tile_AA_button.height);
		g.drawString("Tile AA " + Settings.TILE_ANTI_ALIASING_ENABLED, panel.x + ((panel.width / 2) - (int)(title_font.deriveFont(Font.PLAIN, 24).getStringBounds("Tile AA " + Settings.TILE_ANTI_ALIASING_ENABLED, frc).getWidth() / 2)), tile_AA_button.y + (tile_AA_button.height / 2)+ + (int)(title_font.deriveFont(Font.PLAIN, 24).getStringBounds("Tile AA " + Settings.TILE_ANTI_ALIASING_ENABLED, frc).getHeight() / 2));
		
		//Player Anti Aliasing toggle button
		player_AA_button = new Rectangle((int)(panel.x + (panel.width / 2) - 80), tile_AA_button.y + tile_AA_button.height + 10, 160, 40);
		if(Utils.inBounds(new Point(game.getMousePosition().x, game.getMousePosition().y), player_AA_button)) {
			g.setColor(new Color(255, 255, 255, 40));
			g.fillRect(player_AA_button.x, player_AA_button.y, player_AA_button.width, player_AA_button.height);
		}
		g.setColor(new Color(255, 0, 0, 255));
		g.drawRect(player_AA_button.x, player_AA_button.y, player_AA_button.width, player_AA_button.height);
		g.drawString("Player AA " + Settings.PLAYER_ANTI_ALIASING_ENABLED, panel.x + ((panel.width / 2) - (int)(measureStringWidth(title_font, "Player AA " + Settings.PLAYER_ANTI_ALIASING_ENABLED, 24) / 2)), player_AA_button.y + (player_AA_button.height / 2) + (int)(measureStringHeight(title_font, "Player AA " + Settings.PLAYER_ANTI_ALIASING_ENABLED, 24) / 2));

		//Player Anti Aliasing toggle button
		vsync_button = new Rectangle((int)(panel.x + (panel.width / 2) - 80), player_AA_button.y + player_AA_button.height + 10, 160, 40);
		if(Utils.inBounds(new Point(game.getMousePosition().x, game.getMousePosition().y), vsync_button)) {
			g.setColor(new Color(255, 255, 255, 40));
			g.fillRect(vsync_button.x, vsync_button.y, vsync_button.width, vsync_button.height);
		}
		g.setColor(new Color(255, 0, 0, 255));
		g.drawRect(vsync_button.x, vsync_button.y, vsync_button.width, vsync_button.height);
		g.drawString("VSync " + Settings.VSYNC, panel.x + ((panel.width / 2) - (int)(measureStringWidth(title_font, "VSync " + Settings.VSYNC, 24) / 2)), vsync_button.y + (vsync_button.height / 2) + (int)(measureStringHeight(title_font, "VSync " + Settings.VSYNC, 24) / 2));

		//Exit button
		back_button = new Rectangle((int)(panel.x + (panel.width / 2) - 80), panel.y + panel.height - 80, 160, 40);
		if(Utils.inBounds(new Point(game.getMousePosition().x, game.getMousePosition().y), back_button)) {
			g.setColor(new Color(255, 255, 255, 40));
			g.fillRect(back_button.x, back_button.y, back_button.width, back_button.height);
		}
		g.setColor(new Color(255, 0, 0, 255));
		g.drawRect(back_button.x, back_button.y, back_button.width, back_button.height);
		g.drawString("Back", panel.x + ((panel.width / 2) - (int)(measureStringWidth(title_font, "Back", 24) / 2)), back_button.y + (back_button.height / 2)+ + (int)(measureStringHeight(title_font, "Back", 24) / 2));

		
		//Credits
		g.setFont(Assets.fontList.get("text_font").deriveFont(Font.PLAIN, 10));
		g.drawString("GameX Developed by Kieran Devlin 2015", panel.x + ((panel.width / 2) - (int)(text_font.deriveFont(Font.PLAIN, 10).getStringBounds("GameX Developed by Kieran Devlin 2015", frc).getWidth() / 2)), panel.y + panel.height - 20);
		

	}
	
	@Override
	public void update() {
		
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if(e.getButton() == KeyboardHandler.MouseButtons.LEFT.getValue()) {
			
			if(Utils.inBounds(new Point(e.getX(), e.getY()), resolution_button)) { //Changes the resolution
				int currentResIndex = resolutions.indexOf(game.getWidth() + "x" + game.getHeight()) + 1;
				if(currentResIndex == -1 || currentResIndex == resolutions.size()) {
					currentResIndex = 0;
				}
				game.setResolution((int)Utils.parseResolution(resolutions.get(currentResIndex)).getWidth(), (int)Utils.parseResolution(resolutions.get(currentResIndex)).getHeight());
			}
			
			if(Utils.inBounds(new Point(e.getX(), e.getY()), lighting_button)) {
				Settings.LIGHTING_ENABLED = !Settings.LIGHTING_ENABLED;
			}
			
			if(Utils.inBounds(new Point(e.getX(), e.getY()), lighting_type_button)) {
				if(Settings.ALPHA_LIGHTING_ENABLED) {
					Settings.ALPHA_LIGHTING_ENABLED = false;
					Settings.TILE_LIGHTING_ENABLED = true;
				}else{
					Settings.ALPHA_LIGHTING_ENABLED = true;
					Settings.TILE_LIGHTING_ENABLED = false;
				}
			}
			
			if(Utils.inBounds(new Point(e.getX(), e.getY()), tile_AA_button)) {
				Settings.TILE_ANTI_ALIASING_ENABLED = !Settings.TILE_ANTI_ALIASING_ENABLED;
			}
			
			if(Utils.inBounds(new Point(e.getX(), e.getY()), player_AA_button)) {
				Settings.PLAYER_ANTI_ALIASING_ENABLED = !Settings.PLAYER_ANTI_ALIASING_ENABLED;
			}
			if(Utils.inBounds(new Point(e.getX(), e.getY()), vsync_button)) {
				Settings.VSYNC = !Settings.VSYNC;
			}
			
			if(Utils.inBounds(new Point(e.getX(), e.getY()), back_button)) {
				game.getStateHandler().setState(new MainMenu(game), game.getDisplay().getCanvas());
			}
		}
	}
	
	private double measureStringWidth(Font font, String data, int size) { //Make use off
		return font.deriveFont(Font.PLAIN, size).getStringBounds(data, frc).getWidth();
	}
	
	private double measureStringHeight(Font font, String data, int size) { //Make use off
		return font.deriveFont(Font.PLAIN, size).getStringBounds(data, frc).getHeight();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		game.setMousePosition(new Point(e.getX(), e.getY()));
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
