package dev.gamex.camera;

import java.awt.Point;

import dev.gamex.Game;
import dev.gamex.entity.Entity;

public class GameCamera {

	private float xOffset, yOffset;
	private Game game;
	
	public GameCamera(Game game, float xOffset, float yOffset) {
		this.game = game;
		this.xOffset = xOffset;
		this.yOffset = yOffset;
	}
	
	public float getXOffset() {
		return xOffset;
	}
	
	public float getYOffset() {
		return yOffset;
	}
	
	public Point getOffset() {
		return new Point((int)xOffset, (int)yOffset);
	}
	
	public Point getTile() {
		return new Point((int)((xOffset / 32 ) / game.getScale()), (int)((yOffset / 32) / game.getScale()));
	}
	
	public void setXOffset(int x) {
		xOffset += x;
	}
	
	public void setYOffset(int y) {
		yOffset += y;
	}
	
	public void setOffset(Point p) {
		xOffset += p.getX();
		yOffset += p.getY();
	}
	
	public void focusOnEntity(Entity e) {
		xOffset = (e.getX() - (game.getWidth() / 2));
		yOffset = (e.getY() - (game.getHeight() / 2));
	}
	
	public boolean containsTile(Point tileLoc) {
		if(tileLoc.getY() > (game.getGameCamera().getTile().getY() - 1) && tileLoc.getY() < game.getGameCamera().getTile().getY() + game.getDisplay().getCanvas().getHeight()) {
			if(tileLoc.getX() > (game.getGameCamera().getTile().getX() - 1) && tileLoc.getX() < game.getGameCamera().getTile().getX() + game.getDisplay().getCanvas().getWidth()) {
				return true;
			}
		}
		return false;
	}
	
}
