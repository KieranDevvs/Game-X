package dev.gamex.map;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.VolatileImage;

import dev.gamex.Game;
import dev.gamex.Settings;

public class Tile extends Rectangle {

	private static final long serialVersionUID = 1L;

	private float light = 0f;
	
	private BufferedImage texture;
	@SuppressWarnings("unused")
	private VolatileImage vtexture; //for hardware acceleration. Need to complete.
	private BufferedImage object;
	private Point location;
	private String text;
	private String text2;
	private boolean isSolid = false;
	
	public Tile(BufferedImage texture, Point loc, BufferedImage object, Game game) {
		this.texture = texture;
		this.object = object;
		this.location = loc;
		this.setBounds(this.location.x, this.location.y, (int)(32 * game.getScale()), (int)(32 * game.getScale()));
	}
	
	public Tile(VolatileImage texture, Point loc, BufferedImage object, Game game) {
		this.vtexture = texture;
		this.object = object;
		this.location = loc;
		this.setBounds(this.location.x, this.location.y, (int)(32 * game.getScale()), (int)(32 * game.getScale()));
	}
	
	public Tile(BufferedImage texture, Point loc, Game game) {
		this.texture = texture;
		this.location = loc;
		this.setBounds(this.location.x, this.location.y, (int)(32 * game.getScale()), (int)(32 * game.getScale()));
	}
	
	public Tile( Point loc) {
		this.location = loc;
	}
	
	public void tick() {
		
	}
	
	public void render(Graphics g, Game game, int x, int y) {
		
        Graphics2D g2 = (Graphics2D) g;
        
        if(Settings.TILE_ANTI_ALIASING_ENABLED) {
        	g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        	g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        }
		
		g2.drawImage(texture, (int)((x * 32 * game.getScale()) - game.getGameCamera().getXOffset()), (int)((y * 32 * game.getScale()) - game.getGameCamera().getYOffset()), (int)(32 * game.getScale()), (int)(32 * game.getScale()), null);
		g2.drawImage(object, (int)((x * 32 * game.getScale()) - game.getGameCamera().getXOffset()), (int)((y * 32 * game.getScale()) - game.getGameCamera().getYOffset()), (int)(32 * game.getScale()), (int)(32 * game.getScale()), null);
		//g.setColor(new Color(0, 0, 0, light));
		//g.fillRect((int)((x * 32) - game.getGameCamera().getXOffset()), (int)((y * 32) - game.getGameCamera().getYOffset()), 32, 32);
		
		if(Settings.MAP_GRID) {
			g.setColor(new Color(255, 0, 0, 255));
			g.drawRect((int)((x * 32 * game.getScale()) - game.getGameCamera().getXOffset()), (int)((y * 32 * game.getScale()) - game.getGameCamera().getYOffset()), (int)(32 * game.getScale()), (int)(32 * game.getScale()));
		}
		
		if(text != null) {
			g.setColor(new Color(0, 0, 255, 255));
			g.drawString(text, (int)((x * 32 * game.getScale()) - game.getGameCamera().getXOffset()), (int)((y * 32 * game.getScale()) - game.getGameCamera().getYOffset() + 13));
		}
		
		if(text2 != null) {
			g.setColor(new Color(0, 0, 255, 255));
			g.drawString(text2, (int)((x * 32 * game.getScale()) - game.getGameCamera().getXOffset()), (int)((y * 32 * game.getScale()) - game.getGameCamera().getYOffset() + 23));
		}
	}
	
	public boolean isSolid() {
		return this.isSolid;
	}
	
	public void setSolid(boolean val) {
		this.isSolid = val;
	}
	
	public void setLight(float power) {
		this.light = power;
	}
	
	public float getLight() {
		return light;
	}
	
	public Point getLocation() {
		return location;
	}
	
	public void setObject(BufferedImage object) {
		this.object = object;
	}
	
	public void setText(String text, String text2) {
		this.text = text;
		this.text2 = text2;
	}
	
}
