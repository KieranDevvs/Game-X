package dev.gamex.map;

import java.awt.Graphics;
import java.awt.Point;

import dev.gamex.Game;
import dev.gamex.gfx.Assets;
import dev.gamex.map.lighting.tilelighting.LightTileOverlay;

public class MainMap extends MapHandler {
	
	private Game game;
	private int tilesRendered;
	
	public MainMap(Game game) {
		this.game = game;
	}

	public void tick() {
		
	}
	
	public void render(Graphics g, Game game) {
		
		//Dynamic rendering, only loads chunk that player can view.
		//int pX = game.getPlayer().getTile().x;
		//int pY = game.getPlayer().getTile().y;
		//Rendering from the camera not the player.
		int pX = (int)game.getGameCamera().getTile().x;
		int pY = (int)game.getGameCamera().getTile().y;
		
		setTilesRendered(0);
		
		for(int y = pY; y < pY + (game.getHeight() / (32 * game.getScale())) + 2; y++) {
			for(int x = pX; x < pX + (game.getWidth() / (32 * game.getScale())) + 2; x++) {
				if(!(getTile(new Point(x, y)) == null)) {
					game.getMapHandler().getTile(new Point(x, y)).render(g, game, x, y);
					setTilesRendered(getTilesRendered() + 1);
				}
			}
		}
	}
	
	public void loadMap() {
		getTileList().clear();
		for(int x = 0; x < 100; x++) {
			for(int y = 0; y < 100; y++) {
				game.getMapHandler().getTileList().put(new Point(x, y), new Tile(Assets.imageList.get("grass"), new Point(x, y), game));
				game.getMapHandler().getLightTileList().put(new Point(x, y), new LightTileOverlay(new Point(x, y)));
			}
		}
		game.getMapHandler().getTile(new Point(5, 5)).setObject(Assets.imageList.get("campfire"));
		game.getMapHandler().getTile(new Point(10, 10)).setObject(Assets.imageList.get("campfire"));
		game.getMapHandler().getTile(new Point(10, 10)).setSolid(true);
		game.getMapHandler().getTile(new Point(5, 5)).setSolid(true);
		
	}
	
	public Tile getTile(Point loc) {
		return game.getMapHandler().getTileList().get(loc);
	}
	
	public Tile getTileFromPixel(Point loc) {
		Tile test = game.getMapHandler().getTileList().get(new Point(loc.x / 32, loc.y / 32));
		if(test != null) {
			return test;
		}
		return new Tile(new Point(0,0));
	}
	
	public LightTileOverlay getLightTile(Point loc) {
		return game.getMapHandler().getLightTileList().get(loc);
	}

	@Override
	public int getTilesRendered() {
		return this.tilesRendered;
	}

	@Override
	public void setTilesRendered(int value) {
		this.tilesRendered = value;
	}
	
}
