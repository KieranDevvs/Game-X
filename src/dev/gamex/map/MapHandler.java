package dev.gamex.map;

import java.awt.Graphics;
import java.awt.Point;
import java.util.HashMap;

import dev.gamex.Game;
import dev.gamex.map.lighting.tilelighting.LightTileOverlay;

public abstract class MapHandler {

	public HashMap<Point, Tile> tileList = new HashMap<Point, Tile>();
	public HashMap<Point, LightTileOverlay> lightTileList = new HashMap<Point, LightTileOverlay>();
	
	public static MapHandler currentMap;
	
	public MapHandler() {
		
	}
	
	public abstract void tick();
	
	public abstract void render(Graphics g, Game game);
	
	public abstract int getTilesRendered();

	public abstract void setTilesRendered(int value);
	
	public abstract void loadMap();
	
	public void setMap(MapHandler map) {
		currentMap = map;
		getMap().loadMap();
	}
	
	public MapHandler getMap() {
		return currentMap;
	}
	
	public HashMap<Point, Tile> getTileList() {
		return tileList;
	}
	
	public HashMap<Point, LightTileOverlay> getLightTileList() {
		return lightTileList;
	}
	
	public Tile getTile(Point loc) {
		return tileList.get(loc);
	}
	
	public Tile getTileFromPixel(Point loc) {
		return tileList.get(new Point(loc.x / 32, loc.y / 32));
	}
	
	public LightTileOverlay getLightTile(Point loc) {
		return lightTileList.get(loc);
	}
	
}
