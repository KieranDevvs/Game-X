package dev.gamex.map;

import java.awt.Graphics;
import java.awt.Point;

import dev.gamex.Game;
import dev.gamex.gfx.Assets;

public class SecondMap extends MapHandler {
	
	private Game game;
	
	public SecondMap(Game game) {
		this.game = game;
	}
	
	public void tick() {
		
	}
	
	public void render(Graphics g, Game game) {
		for(int y = 0; y < 40; y++) {
			for(int x = 0; x < 40; x++) {
				getTile(new Point(x, y)).render(g, game, x, y);
			}
		}
	}
	
	public void loadMap() {
		tileList.clear();
		for(int x = 0; x < 40; x++) {
			for(int y = 0; y < 40; y++) {
				tileList.put(new Point(x, y), new Tile(Assets.imageList.get("grass2"), new Point(x, y), game));
			}
		}
	}
	
	public Tile getTile(Point loc) {
		return tileList.get(loc);
	}

	@Override
	public int getTilesRendered() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setTilesRendered(int value) {
		// TODO Auto-generated method stub
		
	}
}
