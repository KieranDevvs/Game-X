package dev.gamex.map.lighting.tilelighting;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;

import dev.gamex.Game;
import dev.gamex.Settings;
import dev.gamex.utils.RGB;

public class LightTileOverlay {

	private float light = 1f;
	private float red = 0f;
	private float green = 0f;
	private float blue = 0f;
	
	private Point location;
	private ArrayList<TileLight> parents = new ArrayList<TileLight>(); 
	private String text;
	private String text2;
	
	
	
	public LightTileOverlay(Point loc) {
		this.location = loc;
	}
	
	public void tick() {
		
	}
	
	public void render(Graphics g, Game game, int x, int y) {
		
		if(Settings.TILE_LIGHTING_COLOUR) {
			g.setColor(new Color(red, green, blue, 0.4f));
			//g.setColor(new Color(1F,0,0, light));
			g.fillRect((int)((x * 32 * game.getScale()) - game.getGameCamera().getXOffset()), (int)((y * 32 * game.getScale()) - game.getGameCamera().getYOffset()), (int)(32 * game.getScale()), (int)(32 * game.getScale()));
		}	
		
		g.setColor(new Color(0,0,0, light));
		g.fillRect((int)((x * 32 * game.getScale()) - game.getGameCamera().getXOffset()), (int)((y * 32 * game.getScale()) - game.getGameCamera().getYOffset()), (int)(32 * game.getScale()), (int)(32 * game.getScale()));
		
		
		if(text != null) {
			g.setColor(new Color(0, 0, 255, 255));
			g.drawString(text, (int)((x * 32 * game.getScale()) - game.getGameCamera().getXOffset()), (int)((y * 32 * game.getScale()) - game.getGameCamera().getYOffset() + 13));
		}
		
		if(text2 != null) {
			g.setColor(new Color(0, 0, 255, 255));
			g.drawString(text2, (int)((x * 32 * game.getScale()) - game.getGameCamera().getXOffset()), (int)((y * 32 * game.getScale()) - game.getGameCamera().getYOffset() + 23));
		}
	}
	
	public boolean isSolid() {
		return false;
	}
	
	public void setLight(float power) {
		this.light = power;
	}
	
	public float getLight() {
		return light;
	}
	
	public void setColour(float r, float g, float b) {
		this.red = r;
		this.green = g;
		this.blue = b;
	}
	
	public RGB getColour() {
		return new RGB(red, green, blue);
	}
	
	public Point getLocation() {
		return this.location;
	}
	
	public void setText(String text, String text2) {
		this.text = text;
		this.text2 = text2;
	}
	
	public void addParent(TileLight parent) {
		this.parents.add(parent);
	}
	
	public ArrayList<TileLight> getParents() {
		return this.parents;
	}
	
	public void removeParent(TileLight parent) {
		this.parents.remove(parent);
	}
	
	public void removeAllParents() {
		this.parents.clear();
	}
	
}
