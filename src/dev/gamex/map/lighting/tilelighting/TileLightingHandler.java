package dev.gamex.map.lighting.tilelighting;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Map.Entry;

import dev.gamex.Game;
import dev.gamex.utils.Utils;

public class TileLightingHandler {

	private Game game;
	private ArrayList<TileLight> lightsList = new ArrayList<TileLight>();
	
	public TileLightingHandler(Game game) {
		this.game = game;
	}
	
	//To-do
	//Make the lighting so that it ranges from light.getBrightness() to 1F(true black).
	//Also add lighting color.
	//Put players and entities under the shadowing. (Done but it doesnt seem right.)
	//Add a night varible to control time of day, this will allow for all shadows, partial or none.
	//Add a way to add a bigger light source or add multiple sources fast. (e.g create rectangle with containing light tiles)
	
	public void update() {
		
		for(Entry<Point, LightTileOverlay> t : game.getMapHandler().getLightTileList().entrySet()) {
			t.getValue().setLight(1F);
			//t.getValue().setText(null, null);
		}
		
		for(TileLight l : lightsList) {
			updateLightTile(l);
		}
		
	}
	
	public void render(Graphics g) {
		int pX = (int)game.getGameCamera().getTile().x;
		int pY = (int)game.getGameCamera().getTile().y;
		
		for(int y = pY; y < pY + (game.getHeight() / (32 * game.getScale())) + 2; y++) {
			for(int x = pX; x < pX + (game.getWidth() / (32 * game.getScale())) + 2; x++) {
				if(!(game.getMapHandler().getLightTile(new Point(x, y)) == null)) {
					game.getMapHandler().getLightTile(new Point(x, y)).render(g, game, x, y);
				}
			}
		}
	}
	
	private void updateLightTile(TileLight l) {
		for(int y = -(l.getDist()); y < l.getDist() - 1; y++) {
			for(int x = -(l.getDist()); x < l.getDist() - 1; x++) {
				
				int distanceFromSource = (Math.abs(x + 1) + Math.abs(y + 1)) + 1;
				LightTileOverlay changer = game.getMapHandler().getLightTile(new Point((int)(l.getTile().getLocation().getX()) + x + 1, (int)l.getTile().getLocation().getY() + y + 1));
				if(changer != null) {
					
					if(game.getGameCamera().containsTile(changer.getLocation())) {
						
								
						
							//if(!(changer.getColour() == l.getColour())) {
								//changer.setColour((changer.getColour().getRed() + l.getColour().getRed()) / 2, (changer.getColour().getGreen() + l.getColour().getGreen()) / 2, (changer.getColour().getBlue() + l.getColour().getBlue()) / 2);
							//}else{
								//changer.setColour(l.getColour().getRed(), l.getColour().getGreen(), l.getColour().getBlue());
							//}
							
							
						
							if(changer.getLight() > (Utils.inverseRange((Utils.inverseRange(l.getBrightness(), 1F) / distanceFromSource), 1F))) {
								//Check to see if the rbg's are different and merge them.
								
								
								changer.setLight((Utils.inverseRange((Utils.inverseRange(l.getBrightness(), 1F) / distanceFromSource), 1F)));
								changer.setColour(l.getColour().getRed(), l.getColour().getGreen(), l.getColour().getBlue());
								//changer.setText(Math.abs(x + 1) + "," + Math.abs(y + 1), "" + (Math.abs(x + 1) + Math.abs(y + 1)));
							}
							
					}
					
					
				}
				
			}
		}
	}
	
	public void load() {
		//Source tile, starting brightness, radius (quality)
		//lightsList.add(new Light(game.getMapHandler().getTile(new Point(3,3)), 0F, 20));
		lightsList.add(new TileLight(game.getMapHandler().getTile(new Point(5,5)), 0F, 15, 0F, 0F, 1F));
		lightsList.add(new TileLight(game.getMapHandler().getTile(new Point(10,10)), 0F, 25, 1F, 0F, 0F));
//		lightsList.add(new Light(game.getMapHandler().getTile(new Point(12,12)), 0F, 20));
//		lightsList.add(new Light(game.getMapHandler().getTile(new Point(14,14)), 0F, 20));
//		lightsList.add(new Light(game.getMapHandler().getTile(new Point(15,12)), 0F, 20));
//		lightsList.add(new Light(game.getMapHandler().getTile(new Point(12,17)), 0F, 20));
//		lightsList.add(new Light(game.getMapHandler().getTile(new Point(13,17)), 0F, 20));
		
		//addLightBox(new Rectangle(15, 15, 25, 25), 0F, 8);
	}
	
	public void addLight(TileLight lightsource) {
		lightsList.add(lightsource);
	}
	
	public void addLightBox(Rectangle area, float brightness, int radius, float r, float b, float g) {
		for(int y = area.y; y < area.getHeight(); y++) {
			for(int x = area.x; x < area.getWidth(); x++) {
				lightsList.add(new TileLight(game.getMapHandler().getTile(new Point(x, y)), brightness, radius, r, g, b));
			}
		}
	}
	
	public void removeLight(TileLight lightsource) {
		lightsList.remove(lightsource);
	}
	
}
