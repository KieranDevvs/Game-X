package dev.gamex.map.lighting.tilelighting;

import dev.gamex.map.Tile;
import dev.gamex.utils.RGB;

public class TileLight {

	private Tile tile;
	private float brightness;
	private int dist;
	private float red = 0;
	private float green = 0;
	private float blue = 0;
	
	public TileLight(Tile tilehook, float brightness, int dist, float r, float g, float b) {
		this.tile = tilehook;
		this.brightness = brightness;
		this.dist = dist;
		this.red = r;
		this.green = g;
		this.blue = b;
	}
	
	public float getBrightness() {
		return brightness;
	}
	
	public Tile getTile() {
		return tile;
	}
	
	public int getDist() {
		return dist;
	}
	
	public void setTileHook(Tile hook) {
		this.tile = hook;
	}
	
	public void setColour(float r, float g, float b) {
		this.red = r;
		this.green = g;
		this.blue = b;
	}
	
	public RGB getColour() {
		return new RGB(red, green, blue);
	}
	
}
