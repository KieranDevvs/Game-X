package dev.gamex.map.lighting;

import java.awt.Graphics;

import dev.gamex.Game;
import dev.gamex.Settings;
import dev.gamex.map.lighting.alphalighting.AlphaLightingHandler;
import dev.gamex.map.lighting.tilelighting.TileLightingHandler;

public class LightingHandler {
	
	private Game game;
	private TileLightingHandler tileLighting;
	private AlphaLightingHandler alphaLighting;
	
	public LightingHandler(Game game) {
		this.game = game;
	}
	
	public void update() {
		if(Settings.ALPHA_LIGHTING_ENABLED) {
			alphaLighting.update();
		}
		if(Settings.TILE_LIGHTING_ENABLED) {
			tileLighting.update();
		}
	}
	
	public void render(Graphics g) {
		if(Settings.LIGHTING_ENABLED) {
			if(Settings.ALPHA_LIGHTING_ENABLED) {
				alphaLighting.render(g);
			}
			if(Settings.TILE_LIGHTING_ENABLED) {
				tileLighting.render(g);
			}
		}
	}
	
	public void load() {
		tileLighting = new TileLightingHandler(game);
		alphaLighting = new AlphaLightingHandler(game);
		tileLighting.load();
		alphaLighting.load();
	}
	
}
