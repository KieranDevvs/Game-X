package dev.gamex.map.lighting.alphalighting;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.RadialGradientPaint;
import java.awt.Rectangle;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.awt.image.VolatileImage;
import java.util.ArrayList;

import dev.gamex.Game;
import dev.gamex.Settings;

public class AlphaLightingHandler {

	private Game game;
	private ArrayList<AlphaLight> lightsList = new ArrayList<AlphaLight>();
	private BufferedImage lightScene;
	
	//Hardware accelarated imaging.
	//http://stackoverflow.com/questions/2374321/how-can-i-create-a-hardware-accelerated-image-with-java2d
	
	GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
	GraphicsConfiguration gc = ge.getDefaultScreenDevice().getDefaultConfiguration();
	VolatileImage vImage;
	
	public AlphaLightingHandler(Game game) {
		this.game = game;
		if(Settings.HARDWARE_ACCELERATION) {
			this.vImage = gc.createCompatibleVolatileImage(game.getWidth(), game.getHeight(), Transparency.TRANSLUCENT); //VImage is hardware accelarated is and much faster for rendering.
		}else{
			this.lightScene = new BufferedImage(game.getWidth(), game.getHeight(), BufferedImage.TYPE_INT_ARGB);
		}
	}
	
	public void update() {
		
	}
	
	public void load() {
		
	}
	
	public void render(Graphics g) {
		
		Graphics2D lg;
		if(Settings.HARDWARE_ACCELERATION) {
			lg = vImage.createGraphics();
		}else{
			lg = (Graphics2D) lightScene.getGraphics();
		}
		
		lg.setColor(new Color(0, 0, 0, 100));
		lg.fillRect(0, 0, game.getWidth(), game.getHeight());

		// Get and install an AlphaComposite to do transparent drawing

		RadialGradientPaint p = new RadialGradientPaint(new Point(100,100), 2.5f, new float[] { 0.0f, 1.0f },
                new Color[] { new Color(255, 0, 0, 0),
                new Color(0, 0, 0, 100) });
        lg.setPaint(p);
        lg.fillOval(100, 100, 50, 50);

		lg.dispose();
		
		
		
		
		
		
		
		    
		g.drawImage(vImage, 0, 0, null);
		
	}
	
	public void addLight(AlphaLight lightsource) {
		lightsList.add(lightsource);
	}
	
	public void addLightBox(Rectangle area, float brightness, int radius, float r, float b, float g) {
		
	}
	
	public void removeLight(AlphaLight lightsource) {
		lightsList.remove(lightsource);
	}
	
}
