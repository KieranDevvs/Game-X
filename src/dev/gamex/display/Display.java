package dev.gamex.display;

import java.awt.Canvas;
import java.awt.Dimension;

import javax.swing.JFrame;

public class Display {
	
	private JFrame frame;
	private int _width, _height;
	private String _title;
	
	private Canvas canvas;
	
	public Display(String title, int width, int height, double _scale) {
		this._title = title;
		this._width = width;
		this._height = height;
		createDisplay();
	}
	
	
	private void createDisplay() {
		frame = new JFrame(_title);
		frame.setSize(_width, _height);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		canvas = new Canvas();
		canvas.setPreferredSize(new Dimension(_width, _height));
		canvas.setMaximumSize(new Dimension(_width, _height));
		canvas.setMinimumSize(new Dimension(_width, _height));
		canvas.setFocusable(false);
		
		frame.add(canvas);
		frame.pack();
	}
	
	public Canvas getCanvas() {
		return canvas;
	}
	
	public JFrame getJFrame() {
		return frame;
	}
	
	public void setWidth(int width) {
		this._width = width;
	}
	
	public void setHeight(int height) {
		this._height = height;
	}
	
	public void updateGameResolution() {
		this.canvas.setPreferredSize(new Dimension(this._width, this._height));
		this.canvas.setMaximumSize(new Dimension(this._width, this._height));
		this.canvas.setMinimumSize(new Dimension(this._width, this._height));
		this.frame.pack();
	}
}
