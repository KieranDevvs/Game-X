package dev.gamex.debug;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;

import dev.gamex.Game;
import dev.gamex.Settings;
import dev.gamex.gfx.Assets;

public class DebugMenu {

	private DecimalFormat fpsFormat = new DecimalFormat("#.00");
	private Game game;
	AffineTransform affinetransform = new AffineTransform();     
	FontRenderContext frc = new FontRenderContext(affinetransform,true,true);
	
	public DebugMenu(Game game) {
		this.game = game;
	}
	
	public void render(Graphics g) {
		
		if(Settings.DEBUG_MENU_ENABLED) {
			Font writefont = Assets.fontList.get("text_font").deriveFont(Font.PLAIN, 10);
			
			ArrayList<Integer> debugWidth = new ArrayList<Integer>();
			debugWidth.add((int)writefont.getStringBounds("FPS: " + fpsFormat.format(game.getFPS()) + " [60]", frc).getWidth());
			debugWidth.add((int)writefont.getStringBounds("Player X: " + Double.toString(game.getPlayer().getX()), frc).getWidth());
			debugWidth.add((int)writefont.getStringBounds("Player Y: " + Double.toString(game.getPlayer().getY()), frc).getWidth());
			debugWidth.add((int)writefont.getStringBounds("Camera X Offset: " + Double.toString(game.getGameCamera().getXOffset()), frc).getWidth());
			debugWidth.add((int)writefont.getStringBounds("Camera Y Offset: " + Double.toString(game.getGameCamera().getYOffset()), frc).getWidth());
			debugWidth.add((int)writefont.getStringBounds("Camera X Tile: " + game.getGameCamera().getTile().x, frc).getWidth());
			debugWidth.add((int)writefont.getStringBounds("Camera Y Tile: " + game.getGameCamera().getTile().y, frc).getWidth());
			debugWidth.add((int)writefont.getStringBounds("Tiles Rendered: " + game.getMapHandler().getMap().getTilesRendered(), frc).getWidth());
			debugWidth.add((int)writefont.getStringBounds("Scale: " + game.getScale(), frc).getWidth());
			debugWidth.add((int)writefont.getStringBounds("Mouse Pos: (" + game.getMousePosition().x + ", " + game.getMousePosition().y + ")", frc).getWidth());
			debugWidth.add((int)writefont.getStringBounds("Projectiles: " + game.getPlayer().projectiles.size(), frc).getWidth());
			
			
			int menuWidth = Collections.max(debugWidth) + 6;
			
			g.setColor(new Color(255, 0, 0, 100));
			g.fillRect(2, 2, menuWidth, 81);
			g.setColor(new Color(255, 0, 0, 255));
			g.drawRect(2, 2, menuWidth, 81);
			g.setFont(writefont);
			g.drawString("FPS: " + fpsFormat.format(game.getFPS()) + " [60]", 5, 10);
			g.drawString("Player X: " + Double.toString(game.getPlayer().getX()), 5, 17);
			g.drawString("Player Y: " + Double.toString(game.getPlayer().getY()), 5, 24);
			g.drawString("Camera X Offset: " + Double.toString(game.getGameCamera().getXOffset()), 5, 31);
			g.drawString("Camera Y Offset: " + Double.toString(game.getGameCamera().getYOffset()), 5, 38);
			g.drawString("Camera X Tile: " + game.getGameCamera().getTile().x, 5, 45);
			g.drawString("Camera Y Tile: " + game.getGameCamera().getTile().y, 5, 52);
			g.drawString("Tiles Rendered: " + game.getMapHandler().getMap().getTilesRendered(), 5, 59);
			g.drawString("Scale: " + game.getScale(), 5, 66);
			g.drawString("Mouse Pos: (" + game.getMousePosition().x + ", " + game.getMousePosition().y + ")", 5, 73);
			g.drawString("Projectiles: " + game.getPlayer().projectiles.size(), 5, 80);
		}
		
	}
	
}
