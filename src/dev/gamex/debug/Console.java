package dev.gamex.debug;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.concurrent.CopyOnWriteArrayList;

import dev.gamex.Game;
import dev.gamex.Settings;
import dev.gamex.gfx.Assets;
import dev.gamex.utils.Utils;

public class Console implements KeyListener {

	private Game game;
	private boolean consoleShown = false;
	private String input = "";
	private CopyOnWriteArrayList<String> output = new CopyOnWriteArrayList<String>();
	
	private String splitter = "->";
	private int offset = 0; //Line offset

	private int boxHeight = 0;
	private int previousListItems = 0;
	
	AffineTransform affinetransform = new AffineTransform();     
	FontRenderContext frc = new FontRenderContext(affinetransform,true,true);
	Font writefont = Assets.fontList.get("text_font").deriveFont(Font.PLAIN, 10);
	
	public Console(Game game) {
		this.game = game;
		log("Console v1.0", new Color(150, 150, 150, 255));
		game.getDisplay().getJFrame().addKeyListener(this);
	}
	
	public void render(Graphics g) {
		if(consoleShown) {
			
			if(!(previousListItems == output.size()) || boxHeight >= 255) {
				boxHeight = 0;
				previousListItems = output.size();
				for (String l : output) {
					boxHeight += ((int)writefont.getStringBounds(l, frc).getHeight());
				}
				if(boxHeight > 225) {
					boxHeight = 225;
				}
			}
			
			g.setColor(new Color(0,0, 100, 90));
			g.fillRect(0, 0, game.getWidth(), boxHeight + 15);
			g.setFont(Assets.fontList.get("text_font").deriveFont(Font.PLAIN, 10));
			g.setColor(new Color(255, 255, 255, 200));
			
			for(int i = offset; i < offset + 25; i++) {
				if(i < output.size()) {
					String lineVal = output.get(i);
					if(lineVal.contains("[col")) {
						int lineRGB = Integer.parseInt(lineVal.split("\\[col")[1].split("]")[0]);
						lineVal = lineVal.split("]")[1];
						g.setColor(new Color(lineRGB));
					}
					g.drawString(lineVal, 10, (i - offset + 1) * (int)writefont.getStringBounds(lineVal, frc).getHeight());
					g.setColor(new Color(255, 255, 255, 200));
				}
			}
			
			if(boxHeight >= 225) { //Scrollbar
				float percent;
				if(offset == 0) {
					 percent = 0;
				}else{
					 percent = ((float)offset / (float)(output.size() - 25));
				}
				g.fillRect(game.getWidth() - 6, Math.max((int)(boxHeight * percent) - 20, 0), 5, 20);
			}
			
			g.drawString(Settings.GAME_NAME + " version " + Settings.VERSION, game.getWidth() - (int)writefont.getStringBounds(Settings.GAME_NAME + " version " + Settings.VERSION, frc).getWidth() - 8, (int)writefont.getStringBounds(Settings.GAME_NAME + " version " + Settings.VERSION, frc).getHeight());
			
			g.drawLine(0, boxHeight + 3, game.getWidth(), boxHeight + 3);
			g.drawString(input, 10, boxHeight + 12);
		}
	}
	
	private void addText(char c) {
		if(consoleShown) {
			if(Utils.isAscii(c)) {
				input = input + c;
			}
		}
	}
	
	private void removeText() {
		if(consoleShown) {
			if(input.length() > 0) {
				input = input.substring(0, input.length() -1);
			}
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == 192) {
			if(Settings.CONSOLE_ENABLED) {
				game.getConsole().toggleConsole();
				return;
			}
		}
		
		if(consoleShown) {
			if(e.getKeyCode() == 40) {
				if((offset < output.size() - 25)) {
					offset++;
				}
			}
			if(e.getKeyCode() == 38) {
				if((offset > 0)) {
					offset--;
				}
			}
			if(e.getKeyCode() == KeyEvent.VK_ENTER) {
				ParseCommand();
			}
			if(e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
				removeText();
			}else{
				addText(e.getKeyChar());
			}
		}
		//System.out.println(e.getKeyCode());
		
	}
	
	public void toggleConsole() {
		this.consoleShown = !this.consoleShown;
	}
	
	private void ParseCommand() {
		input = input.toLowerCase();
		switch(input.split(splitter)[0].toLowerCase()) {
			case "var":
				varCommand(input.split(splitter)[1], input.split(splitter)[2]);
			break;
			case "player":
				playerCommand(input.split(splitter)[1], input.split(splitter)[2] + " " +  input.split(splitter)[3]);
			break;
			default: 
				log("Command '" + input.split(splitter)[0] + "' is not found.");
			break;
		}
		this.input = "";
	}
	
	private void varCommand(String var, String value) {
		switch(var) {
			case "lighting":
				Settings.TILE_LIGHTING_ENABLED = Boolean.parseBoolean(value);
			break;
			case "lighting_colour":
				Settings.TILE_LIGHTING_COLOUR = Boolean.parseBoolean(value);
			break;
			case "scale":
				game.setScale(Float.parseFloat(value));
			break;
		}
	}
	
	private void playerCommand(String var, String value) {
		switch(var) {
			case "teleport":
				if(value.split(" ").length > 1) {
					game.getPlayer().SetLocation(new Point(Integer.parseInt(value.split(" ")[0]) * (int)(32 * game.getScale()), Integer.parseInt(value.split(" ")[1]) * (int)(32 * game.getScale())));
				}
			break;
		}
	}
	
	private void log(String data) {
		output.add(data);
		if(output.size() > 25) {
			offset++;
		}
	}
	
	private void log(String data, Color c) {
		output.add("[col" + c.getRGB() + "]" + data);
		if(output.size() > 25) {
			offset++;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}
	
	public ListIterator<String> getListIterator(ArrayList<String> obj) {
		return obj.listIterator(); // Must be manually synched by user! 
	}
	
	public Iterator<String> getIterator(ArrayList<String> obj) {
		return obj.iterator(); // Must be manually synched by user! 
	}
	
}
