package dev.gamex.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import dev.gamex.Game;
import dev.gamex.states.MainMenu;

public class KeyboardHandler implements KeyListener  {

	private boolean[] keys;
	public boolean up, down, left, right;
	private Game game;
	
	public enum MouseButtons {
		LEFT(1),
		MIDDLE(2),
		RIGHT(3);
		
		private final int id;
		MouseButtons(int id) { this.id = id; }
		public int getValue() { return id; }
	}
	
	public KeyboardHandler(Game game) {
		keys = new boolean[1000]; //Should be 256 but some keyboards support more.
		this.game = game;
	}
	
	public void update() {
		up = keys[KeyEvent.VK_W];
		down = keys[KeyEvent.VK_S];
		left = keys[KeyEvent.VK_A];
		right = keys[KeyEvent.VK_D];
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		keys[e.getKeyCode()] = true;
		
		
		
		
		
		
		if(e.getKeyCode() == 27) {
			game.getStateHandler().setState(new MainMenu(game), game.getDisplay().getCanvas());
		}
		
		if(e.getKeyCode() == 45) { //minus key
			game.setScale(game.getScale() - .10F);
		}
		
		if(e.getKeyCode() == 61) { //plus key
			game.setScale(game.getScale() + .10F);
		}
		
		//System.out.println(e.getKeyCode());
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		keys[e.getKeyCode()] = false;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}

}
