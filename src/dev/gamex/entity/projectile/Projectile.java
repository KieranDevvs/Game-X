package dev.gamex.entity.projectile;

import java.awt.image.BufferedImage;

import dev.gamex.Game;
import dev.gamex.entity.Entity;

public abstract class Projectile extends Entity {

	protected final int xOrigin, yOrigin;
	protected double angle;
	protected BufferedImage texture;
	protected double speed, fireRate, damage;
	protected int range;
	
	
	public Projectile(int x, int y, double dir, int range, Game game) {
		super(x, y, game);
		this.xOrigin = x;
		this.yOrigin = y;
		this.angle = dir;
		this.range = range;
	}
	
	public double calculateDistance() {
		double dist = 0;
		dist = Math.sqrt(Math.abs((xOrigin - x) * (xOrigin - x) + (yOrigin - y) * (yOrigin - y)));
		return dist;
	}

	protected abstract void move();
	public abstract int getRange();
	public abstract boolean isCollided(Game game);

}
