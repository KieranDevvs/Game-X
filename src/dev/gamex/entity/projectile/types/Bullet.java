package dev.gamex.entity.projectile.types;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import dev.gamex.Game;
import dev.gamex.entity.projectile.Projectile;
import dev.gamex.gfx.Assets;
import dev.gamex.utils.Utils;
import dev.gamex.utils.Vec2D;

public class Bullet extends Projectile {

	double test;
	
	public Bullet(int x, int y, double dir, int range, Game game) {
		super(x, y, dir, range, game);
		this.range = range;
		this.damage = 1;
		this.fireRate = 1;
		this.speed = 45; //35
		this.test = dir;
	}

	@Override
	public void update() {
		move();
	}
	
	protected void move() {
		x += speed * Math.cos(angle);
		y += speed * Math.sin(angle);
	}
	
	protected Vec2D moveabs() {
		float tx = x += speed * Math.cos(angle);
		float ty = y += speed * Math.sin(angle);
		return new Vec2D(tx, ty);
	}

	@Override
	public void render(Graphics g, Game game) {
		Graphics2D g2 = (Graphics2D) g;
		AffineTransform at = new AffineTransform();
		
		at.translate(x, y);
		at.rotate(angle - Utils.degreesToRadians((double)90), Assets.getImage("bullet").getWidth() / 2, Assets.getImage("bullet").getHeight() / 2);
		g2.drawImage(Assets.getImage("bullet"), at, null);
	}

	@Override
	public int getRange() {
		return this.range;
	}
	
	public boolean isCollided(Game game) { //X & Y go to negative causing the loop to crash because x is never < endX.

		float endX = moveabs().getX();
		float endY = moveabs().getY();
		
		int t = 0;
		System.out.println("x: " + x + " endX: " + endX);
		System.out.println(Math.cos(angle));
		
		for(float iX = x; iX < endX; t ++) {
//			for(float iY = x; iY < endY; iY += Math.sin(angle)) {
//				System.out.println(game.getMapHandler().getTileFromPixel(new Point((int)iX, (int)iY)).getLocation());
//				if(game.getMapHandler().getTileFromPixel(new Point((int)iX, (int)iY)).isSolid()) {
//					return true;
//				}
//			}
			iX += Math.cos(angle);
		}
		
		//loop the section that the projectile will cover when fired.
		//if the bullet hits a solid within the section then collision true
		//right now the angle is sometimes negative meaning the range is calculated wrong within the loop.
		
		return false;
	}

}
