package dev.gamex.entity.player;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import dev.gamex.Game;
import dev.gamex.Settings;
import dev.gamex.entity.mob.Mob;
import dev.gamex.entity.projectile.Projectile;
import dev.gamex.entity.projectile.types.Bullet;

public class Player extends Mob {

	private Game game;
	private float movmentSpeed = 2f;
	private boolean firing = false;
	
	private BufferedImage texture;
	
	private int xProjectileOffset = -2;
	private int yProjectileOffset = -5;
	
	public List<Projectile> projectiles = new ArrayList<Projectile>();
	
	public Player(Game game, float x, float y, int width, int height, BufferedImage texture) {
		super(x, y, width, height, game);
		this.game = game;
		applyTexture(texture);
		//playerBounds = new Rectangle((int)x, (int)y, width, height);
	}
	
	public Player(Game game, float x, float y, BufferedImage texture) {
		super(x, y, game);
		this.game = game;
		applyTexture(texture);
		//playerBounds = new Rectangle((int)x, (int)y, width, height);
	}
	
	@Override
	public void update() {
		if(game.getInputHandler().up) {
			y -= movmentSpeed * game.getScale();
		}
		if(game.getInputHandler().down) {
			y += movmentSpeed * game.getScale();
		}
		if(game.getInputHandler().left) {
			x -= movmentSpeed * game.getScale();
		}
		if(game.getInputHandler().right) {
			x += movmentSpeed * game.getScale();
		}
		updateProjectile();
	}
	
	private void updateProjectile() {
		if(firing) {
			double dx = game.getMousePosition().x - (x - game.getGameCamera().getXOffset());
        	double dy = game.getMousePosition().y - (y - game.getGameCamera().getYOffset());
        	double dir = Math.atan2(dy, dx);
        	fire((int)(x - game.getGameCamera().getXOffset()) + xProjectileOffset, (int)(y - game.getGameCamera().getYOffset()) + yProjectileOffset, dir);
		}
		Iterator<Projectile> iterator = projectiles.iterator();
        while(iterator.hasNext()) {
        	Projectile p = iterator.next();
//        	if(p.isCollided(game)) {
//        		//iterator.remove();
//        		//System.out.println("ERR");
//        	}
        	if(p.calculateDistance() > p.getRange()) {
        		iterator.remove();
        	}
        }
	}
	
	private void fire(int x, int y, double dir) {
		Random rand = new Random();
		Projectile p = new Bullet(x, y, dir + (rand.nextFloat() * (0.1 - (-0.1)) + (-0.1)), 800, game);
		projectiles.add(p);
	}
	
	public void isFiring(boolean val) {
		firing = val;
	}
	
	public void applyTexture(BufferedImage img) {
		this.texture = img;
	}

	@Override
	public void render(Graphics g, Game game) {
		BufferedImage player = texture;
		
		AffineTransform at = new AffineTransform();
		AffineTransform at2 = new AffineTransform();
		int centerWidth = (int)(x - ((player.getWidth() / 4)) - game.getGameCamera().getXOffset());
		int centerHeight = (int)(y - ((player.getHeight() / 2) ) - game.getGameCamera().getYOffset());
		
		at.translate(centerWidth, centerHeight);
        
        //Image processing
        Graphics2D g2 = (Graphics2D) g;
        if(Settings.PLAYER_ANTI_ALIASING_ENABLED) {
        	g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        	g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        }
        
        //For if the camera is not focused on the player then change the x and y coords instead of using the game frame.
        //double dx = game.getInputHandler().getMousePosition().x - ((int)x );
        //double dy = game.getInputHandler().getMousePosition().y - ((int)y );
        double dx = game.getMousePosition().x - (x - game.getGameCamera().getXOffset());
        double dy = game.getMousePosition().y - (y - game.getGameCamera().getYOffset());
        at.rotate(Math.atan2(dy, dx), ((player.getWidth() / 4)), ((player.getHeight() / 2)));

        at2.scale(game.getScale(), game.getScale());
        at.concatenate(at2);
        
		g2.drawImage(player, at, null);
		
		g2.setColor(Color.RED);
		//g2.drawRect((int)(x - game.getGameCamera().getXOffset() - (player.getWidth() / 4)), (int)(y - game.getGameCamera().getYOffset() - (player.getHeight() / 2)), 25,25);
		g2.drawRect((int)(x - game.getGameCamera().getXOffset()), (int)(y - game.getGameCamera().getYOffset()), 1, 1);
		
		renderProjectiles(g);
		
	}
	
	private void renderProjectiles(Graphics g) {
		for(Projectile p : projectiles) {
			p.update();
			p.render(g, game);
		}
	}

}
