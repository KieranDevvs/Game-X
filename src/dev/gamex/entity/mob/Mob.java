package dev.gamex.entity.mob;

import dev.gamex.Game;
import dev.gamex.entity.Entity;

public abstract class Mob extends Entity {

	protected int health;
	protected Game game;
	
	public Mob(float x, float y, int width, int height, Game game) {
		super(x, y, width, height, game);
		this.game = game;
		health = 100;
	}
	
	public Mob(float x, float y, Game game) {
		super(x, y, game);
		this.game = game;
		health = 100;
	}
	
}
