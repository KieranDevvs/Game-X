package dev.gamex.entity;

import java.awt.Graphics;
import java.awt.Point;

import dev.gamex.Game;


public abstract class Entity {

	protected float x, y;
	protected int width;
	protected int height;
	protected Game game;
	
	public Entity(float x, float y, int width, int height, Game game) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.game = game;
	}
	
	public Entity(float x, float y, Game game) {
		this.x = x;
		this.y = y;
	}
	
	public Entity(int x, int y, Game game) {
		this.x = x;
		this.y = y;
	}
	
	public abstract void update();
	
	public abstract void render(Graphics g, Game game);

	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}
	
	public Point getTile() {
		return new Point((int)(x / (32 * game.getScale())), (int)(y / (32 * game.getScale())));
	}
	
	public Point getPoint() {
		return new Point((int)x, (int)y);
	}
	
	public Point getOffset(Game game) {
		return new Point((int)((getX()) - (game.getWidth() / 2)), (int)((getY() - (game.getHeight() / 2))));
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void SetLocation(Point loc) {
		x = (float)loc.getX();
		y = (float)loc.getY();
	}
	
}
