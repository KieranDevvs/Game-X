package dev.gamex;

public class Settings {

	public static boolean LIGHTING_ENABLED = true;
	public static boolean TILE_LIGHTING_ENABLED = true;
	public static boolean ALPHA_LIGHTING_ENABLED = false;
	public static boolean CONSOLE_ENABLED = true;
	public static boolean DEBUG_MENU_ENABLED = true;
	public static boolean TILE_ANTI_ALIASING_ENABLED = true;
	public static boolean PLAYER_ANTI_ALIASING_ENABLED = true;
	public static boolean MAP_GRID = false;
	public static boolean TILE_LIGHTING_COLOUR = false; //Needs work
	public static boolean ALPHA_LIGHTING_COLOUR = false; 
	public static boolean HARDWARE_ACCELERATION = true; 
	public static boolean VSYNC = true;
	
	
	public static String GAME_NAME = "Game X";
	public static double VERSION = 1.0;
	
	
	public static void ToggleLighting(boolean val) {
		LIGHTING_ENABLED = val;
	}
	
	public static void ToggleConsole(boolean val) {
		CONSOLE_ENABLED = val;
	}
	
	public static void ToggleDebugMenu(boolean val) {
		DEBUG_MENU_ENABLED = val;
	}
	
	public static void ToggleTileAA(boolean val) {
		TILE_ANTI_ALIASING_ENABLED = val;
	}
	
	public static void TogglePlayerAA(boolean val) {
		PLAYER_ANTI_ALIASING_ENABLED = val;
	}
	
	public static void ToggleMapGrid(boolean val) {
		MAP_GRID = val;
	}
}
