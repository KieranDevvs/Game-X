package dev.gamex.overlays;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RadialGradientPaint;

import dev.gamex.Game;

public class NightOverlay extends OverlayHandler {

	@Override
	public void render(Graphics g, Game game) {
		RadialGradientPaint p = new RadialGradientPaint(new Point(game.getDisplay().getCanvas().getWidth() / 2, game.getDisplay().getCanvas().getHeight() / 2), game.getDisplay().getCanvas().getWidth() / 2.5f, new float[] { 0.0f, 1.0f },
                new Color[] { new Color(0, 0, 0, 0),
                new Color(0, 0, 0, 255) });
        Graphics2D g2 = (Graphics2D) g;
        g2.setPaint(p);
        g2.fillRect(0, 0, game.getDisplay().getCanvas().getWidth(), game.getDisplay().getCanvas().getHeight());
	}

	@Override
	public void tick() {
		
	}

}
