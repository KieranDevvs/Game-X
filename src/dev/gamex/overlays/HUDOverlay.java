package dev.gamex.overlays;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;

import dev.gamex.Game;
import dev.gamex.gfx.Assets;

public class HUDOverlay extends OverlayHandler {

	@Override
	public void render(Graphics g, Game game) {
		AffineTransform affinetransform = new AffineTransform();     
		FontRenderContext frc = new FontRenderContext(affinetransform,true,true);   
		
		g.setColor(Color.GRAY);
		g.setFont(Assets.fontList.get("text_font").deriveFont(Font.PLAIN, 10));
		g.fillRect(game.getWidth() - 200 - 12, game.getHeight() - 15 - 30, 200, 16);
		g.drawImage(Assets.imageList.get("shield_icon"), game.getWidth() - 200 - 35, game.getHeight() - 35 - 12, null);
		g.setColor(Color.WHITE);
		g.drawString("Armour: 100", ((game.getWidth() - (200 / 2) - 12) - (int)(Assets.fontList.get("text_font").deriveFont(Font.PLAIN, 10).getStringBounds("Armour: 100", frc).getWidth() / 2)),  (game.getHeight() - (28) - 10) + (int)(Assets.fontList.get("text_font").deriveFont(Font.PLAIN, 10).getStringBounds("Armour: 100", frc).getHeight() / 2));
		
		g.setColor(Color.RED);
		g.setFont(Assets.fontList.get("text_font").deriveFont(Font.PLAIN, 10));
		g.fillRect(game.getWidth() - 200 - 12, game.getHeight() - 15 - 10, 200, 16);
		g.drawImage(Assets.imageList.get("heart_icon"), game.getWidth() - 200 - 35, game.getHeight() - 15 - 12, null);
		g.setColor(Color.WHITE);
		g.drawString("HP: 100", ((game.getWidth() - (200 / 2) - 12) - (int)(Assets.fontList.get("text_font").deriveFont(Font.PLAIN, 10).getStringBounds("HP: 100", frc).getWidth() / 2)),  (game.getHeight() - (16 / 2) - 10) + (int)(Assets.fontList.get("text_font").deriveFont(Font.PLAIN, 10).getStringBounds("HP: 100", frc).getHeight() / 2));
	}

	@Override
	public void tick() {
		
	}

}
