package dev.gamex.overlays;

import java.awt.Graphics;
import java.util.HashMap;
import java.util.SortedSet;
import java.util.TreeSet;

import dev.gamex.Game;

public class OverlayHandler {

	public OverlayHandler() {
		
	}
	
	public enum OverlayType {
		 GREEN, WRINKLED, SWEET,
		 ALL;
		}
	
	private static HashMap<Integer, OverlayType> currentOverlay = new HashMap<Integer, OverlayType>();
	
	public SortedSet<Integer> getShadersArray() {
		SortedSet<Integer> keys = new TreeSet<Integer>(currentOverlay.keySet());
		return keys;
	}
	
	public OverlayType getOverlayType(Integer index) {
		return currentOverlay.get(index);
	}
	
	public void clearOverlays() {
		currentOverlay.clear();
	}
	
	public void removeOverlay(Integer index) {
		currentOverlay.remove(index);
	}
	
	public void setOverlayIndex(Integer index, OverlayType overlay) {
		currentOverlay.put(index, overlay);
	}
	
	public void render(Graphics g, Game game) {
		
	}

	public void tick() {
		
	}
	
}
