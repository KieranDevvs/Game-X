package dev.gamex;


public class Launcher {
	
	public static void main(String[] args) {
		Game game = new Game(Settings.GAME_NAME, 800, 600, 1);
		game.start();
	}
	
}
