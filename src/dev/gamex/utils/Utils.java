package dev.gamex.utils;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.ArrayList;

import dev.gamex.map.lighting.tilelighting.LightTileOverlay;

public class Utils {
	
	static CharsetEncoder asciiEncoder = Charset.forName("US-ASCII").newEncoder();
	
	public static boolean inBounds(Point p, Rectangle rect) {
		if(p.getX() >= rect.getX() && p.getX() <= (rect.getX() +rect.getWidth()) && p.getY() >= rect.getY() && p.getY() <= (rect.getY() + rect.getHeight())) {
			return true;
		}
		return false;
	}
	
	public static float inverseRange(float value, float rangeMax) {
		return (rangeMax - value);
	}
	
	public static Rectangle parseResolution(String res) {
		return new Rectangle(0, 0, Integer.parseInt(res.split("x")[1]), Integer.parseInt(res.split("x")[0]));
	}
	
	public static boolean isAscii(char v) {
		 return asciiEncoder.canEncode(v);
	}
	
	public static boolean isAscii(String v) {
		 return asciiEncoder.canEncode(v);
	}
	
	public static String getBetween(String source, String start, String finish, int index) {
		String fistSplit = source.split(start)[index];
		String secondSplit = fistSplit.split(finish)[0];
		return secondSplit;
	}
	
	public static int floatToRGBValue(float value) {
		return (int)((1/255)*value);
	}
	
	public static float RGBToFloatValue(int value) {
		return (value/255);
	}
	
	public static float fixRGB(int value) {
		int i = (int)((1/255) * value);
		int b = (i/255);
		return b;
	}
	
	public static boolean compareRBG(Color a, Color b) {
		
		return false;
	}
	
	public static Color colorMixer(Color c1, Color c2) {
	    int _r = Math.min((c1.getRed() + c2.getRed()),255);
	    int _g = Math.min((c1.getGreen() + c2.getGreen()),255);
	    int _b = Math.min((c1.getBlue() + c2.getBlue()),255);

	    return new Color(_r, _g, _b);
	}
	
	public static Color aravageMultipleColours(ArrayList<LightTileOverlay> tiles) {
		int redBucket = 0;
		int greenBucket = 0;
		int blueBucket = 0;

		for(LightTileOverlay l : tiles) {
			redBucket += RGBToFloatValue((int)l.getColour().getRed());
			greenBucket += RGBToFloatValue((int)l.getColour().getGreen());
			blueBucket += RGBToFloatValue((int)l.getColour().getBlue());
		}
		
		Color averageColor = new Color(redBucket / tiles.size(),
                greenBucket / tiles.size(),
                blueBucket / tiles.size());
		return averageColor;
	}
	
	public static double round(double unrounded, int precision, int roundingMode) {
	    BigDecimal bd = new BigDecimal(unrounded);
	    BigDecimal rounded = bd.setScale(precision, roundingMode);
	    return rounded.doubleValue();
	}
	
	public static double degreesToRadians(Double degrees) {
		return degrees * 0.0174532925;
	}
	
	public static double radiansToDegrees(Double radians) {
		return radians * 57.2957795;
	}
	
}
