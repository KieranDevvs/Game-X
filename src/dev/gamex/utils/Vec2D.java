package dev.gamex.utils;

import java.awt.Point;

public class Vec2D {

	private float x;
	private float y;
	
	public Vec2D(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public Vec2D(Point p) {
		this.x = (float) p.getX();
		this.y = (float) p.getY();
	}
	
	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}
	
	public void setX(float x) {
		 this.x = x;
	}
	
	public void setY(float y) {
		this.y = y;
	}
	
	
}
