package dev.gamex.utils;

public class RGB {
	float red = 0;
	float green = 0;
	float blue = 0;
	
	public RGB(float r, float g, float b) {
		this.red = r;
		this.green = g;
		this.blue = b;
	}
	
	public float getRed() {
		return red;
	}
	
	public float getGreen() {
		return green;
	}
	
	public float getBlue() {
		return blue;
	}
}
