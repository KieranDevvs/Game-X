package dev.gamex.minimap;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import dev.gamex.Game;

public class Minimap {

	private int width;
	private int height;
	private int screenX;
	private int screenY;
	private int transparency;
	
	private Game game;
	
	public Minimap(int x, int y, int width, int height, int transparency, Game game) {
		this.screenX = x;
		this.screenY = y;
		this.width = width;
		this.height = height;
		this.transparency = transparency;
		this.game = game;
	}
	
	public void update() {
		
	}
	
	public void render(Graphics g) {
		g.setColor(new Color(255, 0, 0, transparency));
		
		int pX = (int)game.getGameCamera().getTile().x;
		int pY = (int)game.getGameCamera().getTile().y;
		
		for(int y = pY; y < pY + (game.getHeight() / (32 * game.getScale())) + 2; y++) {
			for(int x = pX; x < pX + (game.getWidth() / (32 * game.getScale())) + 2; x++) {
				if(!(game.getMapHandler().getTile(new Point(x, y)) == null)) {
					g.drawRect(screenX + (pX * 5), screenY + (y * 5), 5, 5);
				}
			}
		}
		
		g.drawRect(screenX, screenY, width, height);
	}
	
}
