package dev.gamex;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferStrategy;
import java.math.BigDecimal;

import dev.gamex.camera.GameCamera;
import dev.gamex.debug.Console;
import dev.gamex.debug.DebugMenu;
import dev.gamex.display.Display;
import dev.gamex.entity.player.Player;
import dev.gamex.gfx.Assets;
import dev.gamex.input.KeyboardHandler;
import dev.gamex.interfaces.InterfaceHandler;
import dev.gamex.map.MainMap;
import dev.gamex.map.MapHandler;
import dev.gamex.map.lighting.LightingHandler;
import dev.gamex.states.LoadingMenu;
import dev.gamex.states.MainMenu;
import dev.gamex.states.StateHandler;
import dev.gamex.utils.Utils;

public class Game implements Runnable {

	private Display display;
	private int _width, _height;
	private double _scale;
	private String _title;
	
	private int mouseX = 0;
	private int mouseY = 0;

	private double actualFPS;
	
	private Thread thread;
	private boolean running = false;
	
	private BufferStrategy _bufferStrategy;
	private Graphics g;
	
	private KeyboardHandler inputListner;
	
	private DebugMenu debugmenu;
	private Console console;
	
	private StateHandler gameState = new MainMenu(this); //Set to main menu to declare the variable
	private MapHandler mapHandler = new MainMap(this); 
	private InterfaceHandler interfaceHandler;
	private Player player = new Player(this, 500, 500, 32, 32, Assets.getImage("player"));
	
	private GameCamera playerCamera;
	
	private LightingHandler lighting;
	
	public Game(String title, int width, int height, int scale) {
		this._title = title;
		this._width = width;
		this._height = height;
		this._scale = scale;
	}
	
	private void init() {
		System.setProperty("sun.java2d.opengl", "true"); //Removes fps hook, performance to be tested.
		System.setProperty("sun.java2d.translaccel", "true");
		System.setProperty("sun.java2d.ddforcevram", "true"); 
		
		display = new Display(_title, _width, _height, _scale); 
		gameState.setState(new LoadingMenu(this), display.getCanvas());
		LoadingMenu LoadingScrn = (LoadingMenu) gameState.getState();
		
		LoadingScrn.setLoadBar(0, "Loading Assets, Please wait...");
		Assets.init();
		
		//LoadingSrn.setLoadBar(10, "Loading Icons...");
		getDisplay().getJFrame().setIconImages(Assets.frameIcons);
		
		//LoadingSrn.setLoadBar(20, "Loading input handler...");
		inputListner = new KeyboardHandler(this);
		
		//LoadingSrn.setLoadBar(30, "Loading Debug menu...");
		debugmenu = new DebugMenu(this);
		console = new Console(this);
		
		//LoadingSrn.setLoadBar(40, "Loading Camera...");
		playerCamera = new GameCamera(this, 0, 0);
		
		player = new Player(this, 500, 500, 32, 32, Assets.getImage("player"));
		
		//LoadingSrn.setLoadBar(50, "Loading Interface...");
		//getInterfaceHandler().setInterface(new BlankInterface(), getDisplay().getJFrame(), getDisplay().getCanvas());
		
		//LoadingSrn.setLoadBar(60, "Loading key listner...");
		getDisplay().getJFrame().addKeyListener(inputListner);
		
		//LoadingSrn.setLoadBar(70, "Loading Maps...");
		getMapHandler().setMap(new MainMap(this));
		
		//LoadingSrn.setLoadBar(90, "Loading Lighting handler...");
		lighting = new LightingHandler(this);
		lighting.load();
		
		//LoadingSrn.setLoadBar(100, "Done, Please wait...");
		//gameState.setState(new MainMenu(this), display.getCanvas());
	}
	
	private void tick() {
		getInputManager().update();
		getStateHandler().getState().update();
	}
	
	private void render() {
		_bufferStrategy = display.getCanvas().getBufferStrategy();
		if(_bufferStrategy == null) {
			display.getCanvas().createBufferStrategy(3);
			return;
		}
		g = _bufferStrategy.getDrawGraphics();
		
		g.clearRect(0, 0, _width, _height);
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, _width, _height);
		
		getStateHandler().getState().render(g, this);
		//for(Integer shaderIndex : getOverlayHandler().getShadersArray()) {
			//getOverlayHandler().getShader(shaderIndex).render(g, this);
		//}
		
		debugmenu.render(g);
		console.render(g);
		
		_bufferStrategy.show();
		g.dispose();
	}
	
	public void run() {
		init();
		
		int fps = 60;
		double timePerTick = 1000000000 / fps;
		double delta = 0;
		long now;
		long lastTime = System.nanoTime();
		
		long lastframe = System.currentTimeMillis();
		long rendered = 0;
		
		while(running) {
			now = System.nanoTime();
			delta += (now - lastTime) / timePerTick;
			lastTime = now;
			
			if(delta >= 1) {
				tick();
				if(Settings.VSYNC) {
					render();
					rendered++;
				}
	            delta--;
			}
            if ((System.currentTimeMillis() - lastframe) > 1000) {
            	actualFPS = (Math.ceil(rendered * 1000) / (System.currentTimeMillis() - lastframe));
            	lastframe = System.currentTimeMillis();
				rendered = 0;
            }
			if(!(Settings.VSYNC)) {
				render();
				rendered++;
			}
			
		}
		
		stop();
	}
	
	public int getWidth() {
		return _width;
	}
	
	public int getHeight() {
		return _height;
	}
	
	public double getScale() {
		return Utils.round(_scale, 3, BigDecimal.ROUND_HALF_UP);
	}
	
	public void setScale(double d) {
		 _scale = Utils.round(d, 3, BigDecimal.ROUND_HALF_UP);
	}
	
	public Display getDisplay() {
		return display;
	}
	
	public void setResolution(int height, int width) {
		this._width = width;
		this._height = height;
		getDisplay().setWidth(width);
		getDisplay().setHeight(height);
		getDisplay().updateGameResolution();
	}
	
	public KeyboardHandler getInputHandler() {
		return inputListner;
	}
	
	public StateHandler getStateHandler() {
		return gameState;
	}
	
	public LightingHandler getLightingHandler() {
		return lighting;
	}
	
//	public OverlayHandler getOverlayHandler() {
//		return overlay;
//	}
	
	public KeyboardHandler getInputManager() {
		return inputListner;
	}
	
	public GameCamera getGameCamera() {
		return playerCamera;
	}
	
	public InterfaceHandler getInterfaceHandler() {
		return interfaceHandler;
	}
	
	public MapHandler getMapHandler() {
		return mapHandler;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public double getFPS() {
		return actualFPS;
	}
	
	public Console getConsole() {
		return console;
	}
	
	public Point getMousePosition() {
		return new Point(mouseX, mouseY);
	}
	
	public void setMousePosition(Point p) {
		mouseX = p.x;
		mouseY = p.y;
	}
	
	public void setMouseX(int p) {
		mouseX = p;
	}
	
	public void setMousey(int p) {
		mouseY = p;
	}
	
	public synchronized void start() {
		if(running) {
			System.out.println("Game thread '" + this.getClass().getName() + "' is already running.");
		}else{
			running = true;
			thread = new Thread(this);
			thread.start();
		}
	}
	
	public synchronized void stop() {
		if(!running) {
			System.out.println("Game thread '" + this.getClass().getName() + "' has already stopped.");
		}else{
			running = false;
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
}
