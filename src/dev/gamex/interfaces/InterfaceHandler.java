package dev.gamex.interfaces;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;

import javax.swing.JFrame;

import dev.gamex.Game;

public abstract class InterfaceHandler implements KeyListener, MouseListener {

	public InterfaceHandler() {
		
	}
	
	private static InterfaceHandler currentInterface = null;
	
	public InterfaceHandler getInterface() {
		return currentInterface;
	}
	
	public void setInterface(InterfaceHandler interfaceobj, JFrame frame, Canvas canvas) {
		frame.removeKeyListener(currentInterface);
		canvas.removeMouseListener(currentInterface);
		currentInterface = interfaceobj;
		frame.addKeyListener(interfaceobj);
		canvas.addMouseListener(interfaceobj);
	}
	
	public abstract void render(Graphics g, Game game);

	public abstract void tick();
	
}

